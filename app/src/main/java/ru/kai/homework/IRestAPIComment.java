package ru.kai.homework;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import ru.kai.homework.Models.CommentModel;

/**
 * Created by akabanov on 11.10.2017.
 */

public interface IRestAPIComment {
    @GET("{param}/{num}")
    Call<CommentModel> loadData(@Path("param") String param, @Path("num") int num);
}
