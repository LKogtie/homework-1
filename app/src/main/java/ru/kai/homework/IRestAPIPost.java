package ru.kai.homework;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import ru.kai.homework.Models.PostModel;

/**
 * Created by akabanov on 11.10.2017.
 */

public interface IRestAPIPost {
    @GET("{param}/{num}")
    Call<PostModel> loadData(@Path("param") String param, @Path("num") int num);
}
