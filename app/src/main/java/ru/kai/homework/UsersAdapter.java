package ru.kai.homework;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.kai.homework.Models.UserModel;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.UserViewHolder> {

    private List<UserModel> usersList;
    private final LayoutInflater layoutInflater;


    UsersAdapter(Context context) {
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.usersList = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        return (this.usersList != null) ? this.usersList.size() : 0;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = this.layoutInflater.inflate(R.layout.card_view_users, parent, false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, final int position) {
        final UserModel userModel = this.usersList.get(position);

        holder.tvCVUsers.append("\nname = " + userModel.getName()+
                "\nusername = " + userModel.getUserName()+
                "\nemail" + userModel.getEmail());
                //+"\n-----------------");
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setUsersList(List<UserModel> usersList) {
        this.usersList = (List<UserModel>) usersList;
        this.notifyDataSetChanged();
    }

    static class UserViewHolder extends RecyclerView.ViewHolder {
        TextView tvCVUsers;

        UserViewHolder(View itemView) {
            super(itemView);
            tvCVUsers = (TextView) itemView.findViewById(R.id.tvCVUsers);
        }
    }
}
