package ru.kai.homework;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ru.kai.homework.Models.UserModel;

public class FragmentMain extends Fragment{

    RelativeLayout rl_progress;
    RecyclerView rvUsers;
    UsersAdapter usersAdapter;

    CardView cvPost;
    EditText editCVPost;
    TextView tvPost;
    Button btnPost;

    CardView cvComment;
    EditText editComment;
    TextView tvComment;
    Button btnComment;

    ImageView imageCVPhoto;
    TextView tvCVTodos;

    FMPresenter presenter;

    public FragmentMain() {
        // Required empty public constructor
        setRetainInstance(true);
    }

    public static FragmentMain newInstance(Bundle bundle) {
        FragmentMain currentFragment = new FragmentMain();
        Bundle args = new Bundle();
        args.putBundle("gettedArgs", bundle);
        currentFragment.setArguments(args);
        return currentFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new FMPresenter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        initializeView(rootView);

        rvUsers.setLayoutManager(new LinearLayoutManager(getActivity()));
        usersAdapter = new UsersAdapter(getActivity());
        rvUsers.setAdapter(usersAdapter);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.setView(this);
        if(savedInstanceState == null){
            if(presenter.initializeComponents()){
                presenter.loadUsers();
                presenter.loadImage();
                presenter.loadTodo();
            }
        }
    }

    private void initializeView(View rootView){

        rl_progress = (RelativeLayout)rootView.findViewById(R.id.rl_progress);
        rvUsers = (RecyclerView) rootView.findViewById(R.id.rvUsers);

        cvPost = (CardView)rootView.findViewById(R.id.cvPost);
        editCVPost = (EditText) cvPost.findViewById(R.id.editCVWithButton);
        tvPost = (TextView) cvPost.findViewById(R.id.tvCVWithButtonPost);
        btnPost = (Button) cvPost.findViewById(R.id.btnCVWithButton);

        cvComment = (CardView)rootView.findViewById(R.id.cvComment);
        editComment = (EditText) cvComment.findViewById(R.id.editCVWithButton);
        tvComment = (TextView) cvComment.findViewById(R.id.tvCVWithButtonPost);
        btnComment = (Button) cvComment.findViewById(R.id.btnCVWithButton);
        TextView tvCommentCaption = (TextView) cvComment.findViewById(R.id.tvCVWithButtonPostCaption);
        editComment.setHint(R.string.enter_comment_hint);
        tvCommentCaption.setText(R.string.comment_caption);

        imageCVPhoto = (ImageView)rootView.findViewById(R.id.imageCVPhoto);
        tvCVTodos = (TextView)rootView.findViewById(R.id.tvCVTodos);

        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.loadPost(editCVPost.getText().toString());
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(btnPost.getWindowToken(), 0);
            }
        });

        btnComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.loadComment(editComment.getText().toString());
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(btnPost.getWindowToken(), 0);
            }
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        rvUsers.setAdapter(null);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.presenter.destroy();
    }

    public void showLoading() {
        this.rl_progress.setVisibility(View.VISIBLE);
        //this.getActivity().setProgressBarIndeterminateVisibility(true);
    }

    public void hideLoading() {
        this.rl_progress.setVisibility(View.GONE);
        this.getActivity().setProgressBarIndeterminateVisibility(false);
    }

    public void renderUserList(List<UserModel> userList) {
        if (userList != null) {
            this.usersAdapter.setUsersList(userList);
        }
    }

    public void renderTodo(String todo) {
        tvCVTodos.setText(todo);
    }

    public void renderPost(String postBody) {
        tvPost.setText(postBody);
        hideLoading();
    }

    public void renderComment(String commentBody) {
        tvComment.setText(commentBody);
        hideLoading();
    }

    public void setServiceMessage(String msg){
        Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.navigation), msg, Snackbar.LENGTH_LONG);
        snackbar.getView().setBackgroundColor(Color.MAGENTA);
        snackbar.show();
        hideLoading();  // нужно решать с параллельными потоками
    }

    public void loadImage(String uri){
        Picasso.with(getActivity().getApplicationContext())
                .load(uri)
//                .fit()
//                .placeholder(R.drawable.user_placeholder)
//                .error(R.drawable.user_placeholder_error)
                .into(imageCVPhoto);

        //hideLoading(); // нужно решать с параллельными потоками
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
