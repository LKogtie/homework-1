package ru.kai.homework.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Class that represents a user in the presentation layer.
 * сеттеры не используем, так как по условиям задачи - только вывод списка
 */
public class PostModel{

  @SerializedName("id")
  @Expose
  private final int postId;

  @SerializedName("title")
  @Expose
  private String title;
  @SerializedName("body")
  @Expose
  private String body;

  public PostModel(int postId) {
    this.postId = postId;
  }

  public String getTitle() {
    return this.title;
  }

  public String getBody() {
    return this.body;
  }


}
