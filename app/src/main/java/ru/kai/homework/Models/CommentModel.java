package ru.kai.homework.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Class that represents a user in the presentation layer.
 * сеттеры не используем, так как по условиям задачи - только вывод списка
 */
public class CommentModel {

  @SerializedName("id")
  @Expose
  private final int commentId;

  public CommentModel(int commentId) {
    this.commentId = commentId;
  }

  @SerializedName("name")
  @Expose
  private String name;
  @SerializedName("body")
  @Expose
  private String body;
  @SerializedName("email")
  @Expose
  private String email;

  public int getUserId() {
    return this.commentId;
  }

  public String getName() {
    return this.name;
  }

  public String getBody() {
    return this.body;
  }

  public String getEmail() {
    return this.email;
  }

}
